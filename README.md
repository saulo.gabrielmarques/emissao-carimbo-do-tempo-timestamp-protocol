# Emissão de Carimbo do Tempo com Autenticação Básica

Este é um exemplo de integração dos serviços da API de Carimbo do Tempo com clientes baseados em tecnologia C# para emissão de carimbo do tempo utilizando autenticação básica e requisição conforme especificação da RFC 3161. 

### Tech

O exemplo utiliza as bibliotecas C# abaixo:
* [Bouncy Castle C#] - The Bouncy Castle C# APIs for CMS, PKCS, EAC, TSP, CMP, CRMF, OCSP, and certificate generation.

### Variáveis que devem ser configuradas

O exemplo necessita ser configurado com uma credencial (client_id/client_secret) válida.

Os passos para obter a credencial de acesso estão descritos em [Docs BRy para Desenvolvedores](https://api-assinatura.bry.com.br/api-carimbo-do-tempo-basico#autenticacao-basica-timestamp-protocol)

**Observação**

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente à emissão do carimbo do tempo.

| Variável | Descrição | Classe de Configuração |
| ------ | ------ | ------ |
| CLIENT_ID | Client id definido na credencial. | bry-carimbo-exemplo-bc
| CLIENT_SECRET | Client secret definido na credencial | bry-carimbo-exemplo-bc

### Uso

    - Passo 1: Execute o Build da aplicação utilizando o Microsoft Visual Studio.
	- Passo 2: Configure a credencial de acesso (client_id/client_secret) no arquivo de exemplo: bry-carimbo-exemplo-bc.cs.
    - Passo 3: Execute o exemplo.

   [Bouncy Castle C#]: <http://www.bouncycastle.org/csharp/>